let app = new Vue({
  el: "#app",
  data: {
    listComments: [
      {
        body: "Komentar Pertama",
        date: "29/07/2020",
        score: 0,
        like: false,
        dislike: false
      },
      {
        body: "Komentar Kedua",
        date: "28/07/2020",
        score: 5,
        like: false,
        dislike: false
      },
      {
        body: "Komentar Ketiga",
        date: "27/07/2020",
        score: 5,
        like: false,
        dislike: false
      }
    ],
    newComment: ""
  },
  methods: {
    // greet: function (event) {
    //   alert ('Hello !')
    //   if (event) {
    //     alert(event.target.tagName)
    //   }
    // },
    addComment: function () {
      let tempComment = {
        body: this.newComment,
        score: 0,
        date: "29/07/2020",
        like: false,
        dislike: false
      }

      this.listComments.push(tempComment)
      this.newComment = ""
    },
    voteLike: function (index) {

      this.listComments[index].like = this.listComments[index].like ? false : true
      this.listComments[index].dislike = this.listComments[index].dislike ? false : true

      if (this.listComments[index].like &&
        this.listComments[index].dislike) {
        this.listComments[index].score++
        this.listComments[index].like = true
      } else if (
        this.listComments[index].like &&
        !this.listComments[index].disLike
      ) {
        this.listComments[index].score += 2
        this.listComments[index].like = true
        this.listComments[index].dislike = false
      } else {
        this.listComments[index].score--
        this.listComments[index].disLike = false
      }
    },
    voteDisLike: function (index) {
      this.listComments[index].like = this.listComments[index].like ? false : true
      this.listComments[index].dislike = this.listComments[index].dislike ? false : true

      if (this.listComments[index].like &&
        this.listComments[index].dislike ) {
        this.listComments[index].score--
        this.listComments[index].disLike = true
      } else if (
        this.listComments[index].disLike &&
        !this.listComments[index].like
      ) {
        this.listComments[index].score -= 2
        this.listComments[index].disLike = true
        this.listComments[index].like = false
      } else {
        this.listComments[index].score++
        this.listComments[index].disLike = false
      }
    }
  }
})