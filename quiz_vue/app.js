var app = new Vue({
    el: "#app",
    data: {
       name: '',
       isEdit: false, 
       button_text: 'Add',
       users: [
           {
               'name': 'Muhammad Iqbal Mubarok',
           },
           {
               'name': 'Ruby Purwanti',
           },
           {
               'name': 'Faqih Muhammad',
           }
       ] 
    },
    methods: {
        addUser: function() {
            if (this.name != '') {
                this.users.push({name: this.name})
                this.name = ''   
            }
            else {
                alert("Silahkan isikan nama anda")
            }
        },
        editUser: function(index) {
            this.isEdit = true,
            this.button_text = 'Update'
            this.name = this.users[index].name
            Vue.set(this.users[index], 'name', this.name)
        },
        removeUser: function(index) {
            let result = confirm("Anda Yakin ?")
            if (result) {
               this.users.splice(index, 1) 
            }
        }
    }
})