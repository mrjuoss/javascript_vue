// Soal Nomor 2
// Sederhanakan menjadi Object literal di ES6

const newFunction = function literal(firstName, lastName) {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function () {
      console.log(firstName + " " + lastName)
      return
    }
  }
}

//Driver Code
newFunction("William", "Imoh").fullName()

const objectLiteral = (firstName, lastName) => {

  return {
    firstName,
    lastName,
    fullName : () => {
      console.log(firstName + " " + lastName)
    }
  }

}

console.log("Jawaban Soal Nomor 2")

objectLiteral("William", "Imoh").fullName()

console.log("===============================")