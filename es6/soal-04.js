// Soal Nomor 4
// Array Spreading

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// Driver Code

console.log("Jawaban Soal Nomor 4")

const combined = [...west, ...east]

console.log(combined)

console.log("===============================")