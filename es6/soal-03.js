// Soal Nomor 3
// Destructuring

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const { firstName, lastName, destination, occupation, spell } = newObject

console.log("Jawaban Soal No 3")
console.log(firstName, lastName, destination, occupation)

console.log("===============================")