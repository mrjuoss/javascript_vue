// Soal Nomor 5
// Template Literals

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet + ' do eiusmod tempor ' + 'incididunt ut labore et dolore magna aliqua. Ut enim' + ' ad minim veniam'
// Driver Code
console.log(before)

let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam `

console.log("Jawaban Soal Nomor 5")

console.log(after);