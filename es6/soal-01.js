// Soal Nomor 1
// Mengubah fungsi menjadi fungsi arrow

const goldenNormal = function goldenFunction() {
  console.log("this is golden!!")
}

goldenNormal()

// Jawaban
console.log("Jawaban Soal Nomor 1")
const goldenEs6 = () => { console.log("This is golden!!") }

goldenEs6()

console.log("===============================")